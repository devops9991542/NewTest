import { Component } from '@angular/core';
import { Task } from '../../models/task.model';
import { TaskService } from '../../services/task.service';
import { List } from '../../models/list.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrl: './main-content.component.scss'
})
export class MainContentComponent {
  lists!: List[];
  tasks!: Task[];
  title!: string;
  detail!: string;
  createdAt!: Date
  dueDate!: Date

  selectedListId!: string;

  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router, private authService:AuthService) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        if (params['listId']) {
          this.selectedListId = params['listId'];
          this.taskService.getTasks(params['listId']).subscribe((tasks: Task[]) => {
            this.tasks = tasks;
          })
        }
      }
    )

    this.taskService.getLists().subscribe((lists: List[]) => {
      this.lists = lists;
    })
  }




  onToggleTaskStatus(task: Task) {
    // we want to set the task to completed
    this.taskService.complete(task).subscribe(() => {
      // the task has been set to completed successfully
      console.log("Completed successully!");
      task.completed = !task.completed;
    })!
  }

  onDeleteListClick() {
    if(window.confirm('Voulez vous vraiment supprimer cette tâche? Cette action va supprimer toutes les tâches de la liste')){
    this.taskService.deleteList(this.selectedListId).subscribe((res: any) => {
      this.router.navigate(['/lists']);
      console.log(res);
    })
  }
  }

  onDeleteTaskClick(id: string) {
    if(window.confirm('Voulez vous vraiment supprimer cette tâche ?')){
      this.taskService.deleteTask(this.selectedListId, id).subscribe((res: any) => {
        this.tasks = this.tasks.filter(val => val._id !== id);
        console.log(res);
      })
     }
    
  }


  onTaskClick(taskId:string){
    for(const element of this.tasks){
      if(taskId === element._id){
        this.title = element.title,
        this.detail = element.detail,
        this.createdAt = element.createdAt,
        this.dueDate = element.dueDate
      }
    }
  }

  onLogout(){
    this.authService.logout()
  }
}