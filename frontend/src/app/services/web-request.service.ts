import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class WebRequestService {
  readonly apiUrl: string = environment.apiUrl;

  constructor(private http:HttpClient) {

  }


  get(uri:string){
    return this.http.get(`${this.apiUrl}/${uri}`);
  }

  post(uri: string, payload: Object) {
    return this.http.post(`${this.apiUrl}/${uri}`, payload);
  }

  patch(uri: string, payload: Object) {
    return this.http.patch(`${this.apiUrl}/${uri}`, payload);
  }

  delete(uri: string) {
    return this.http.delete(`${this.apiUrl}/${uri}`);
  }


  // For Auth

  login(email: string, password: string) {
    return this.http.post(`${this.apiUrl}/users/login`, {
      email,
      password
    }, {
        observe: 'response'
      });
  }

  signup(email: string, password: string) {
    return this.http.post(`${this.apiUrl}/users`, {
      email,
      password
    }, {
        observe: 'response'
      });
  }

}
