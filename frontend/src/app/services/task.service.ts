import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { Task } from '../models/task.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private webRequestService:WebRequestService) { }

  getLists():Observable<any> {
    return this.webRequestService.get('lists');
  }

  createList(title: string):Observable<any> {
    // We want to send a web request to create a list
    return this.webRequestService.post('lists', { title });
  }

  updateList(id: string, title: string) {
    return this.webRequestService.patch(`lists/${id}`, { title });
  }

  updateTask(listId: string, taskId: string, title: string, detail: string, dueDate: Date) {
    return this.webRequestService.patch(`lists/${listId}/tasks/${taskId}`, { 
      title:title,
      detail: detail,
      dueDate: dueDate
      
     });
  }

  deleteTask(listId: string, taskId: string) {
    return this.webRequestService.delete(`lists/${listId}/tasks/${taskId}`);
  }

  deleteList(id: string) {
    return this.webRequestService.delete(`lists/${id}`);
  }

  getTasks(listId: string): Observable<any> {
    return this.webRequestService.get(`lists/${listId}/tasks`);
  }

  getSingleTask(listId: string, taskId: string):Observable<any> {
    return this.webRequestService.delete(`lists/${listId}/tasks/${taskId}`);
  }

  createTask(title: string, listId: string, detail: string, dueDate: Date ):Observable<any> {
    return this.webRequestService.post(`lists/${listId}/tasks`, { 
      title: title,
      detail: detail,
      dueDate: dueDate
    });
  }

  complete(task: Task) {
    return this.webRequestService.patch(`lists/${task._listId}/tasks/${task._id}`, {
      completed: !task.completed
    });
  }
}