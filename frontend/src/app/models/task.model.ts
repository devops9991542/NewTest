export class Task {
    _id!: string;
    _listId!: string;
    detail!: string;
    dueDate!: Date;
    title!: string;
    createdAt!: Date;
    completed!: boolean;
}