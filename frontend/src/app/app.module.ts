import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EditListComponent } from './components/edit-list/edit-list.component';
import { AddListComponent } from './components/add-list/add-list.component';
import { AddTaskComponent } from './components/add-task/add-task.component';
import * as fr from '@angular/common/locales/fr';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { EditTaskComponent } from './components/edit-task/edit-task.component';
import { MainContentComponent } from './components/main-content/main-content.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { WebRequestInterceptor } from './services/web-request.interceptor';
import { registerLocaleData } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    EditListComponent,
    AddListComponent,
    AddTaskComponent,
    LoginComponent,
    SignupComponent,
    EditTaskComponent,
    MainContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: WebRequestInterceptor, multi: true},
    { provide: LOCALE_ID, useValue: 'fr-FR' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    registerLocaleData(fr.default) // Langue FR (toujours)
  }
}
