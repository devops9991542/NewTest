const express = require('express');
const { authenticate } = require('../middleware/auth');
const taskController = require('../controllers/taskController');

const router = express.Router();



router.get('/lists/:listId/tasks', authenticate, taskController.getAllTasksInList);
router.get('/lists/:listId/tasks/:taskId', authenticate, taskController.getSingleTask);
router.post('/lists/:listId/tasks/', authenticate, taskController.createTask);
router.patch('/lists/:listId/tasks/:taskId', authenticate, taskController.updateTask);
router.delete('/lists/:listId/tasks/:taskId', authenticate, taskController.deleteTask);

module.exports = router;