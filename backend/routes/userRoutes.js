const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const { verifySession } = require('../middleware/auth');


router.post('/login/', userController.login);
router.post('/', userController.signup);
router.get('/me/access-token/', verifySession, userController.getAccessToken);


module.exports = router;