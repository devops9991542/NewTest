const express = require('express');
const listController = require('../controllers/listController');
const { authenticate } = require('../middleware/auth');
const router = express.Router();



router.get('/', authenticate, listController.getAllLists);
router.post('/', authenticate, listController.createList);
router.patch('/:id', authenticate, listController.updateList);
router.delete('/:id', authenticate, listController.deleteList);

module.exports = router;