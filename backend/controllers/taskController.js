const { Task, List }= require('../models');

// GET All Tasks @listId
exports.getAllTasksInList = async (req, res) => {
    Task.find({
        _listId: req.params.listId
    }).then((tasks) => {
        res.send(tasks);
    })
};
// Return Array of lists in database
exports.getSingleTask = async (req, res, next) => {
    Task.findOne({
        _id: req.params.taskId,
        _listId: req.params.listId
    }).then((task) => {
        res.send(task);
    });
}

// POST Create New Task @listId @taskId
exports.createTask = async (req, res) => {
    List.findOne({
        _id: req.params.listId,
        _userId: req.user_id
    }).then((list) => {
        if (list) {
            return true;
        }

        // else - the list object is undefined
        return false;
    }).then((canCreateTask) => {
        if (canCreateTask) {
            let newTask = new Task({
                title: req.body.title,
                _listId: req.params.listId,
                detail:req.body.detail,
                dueDate: req.body.dueDate,
                createdAt: Date.now()
            });
            newTask.save().then((newTaskDoc) => {
                res.send(newTaskDoc);
            })
        } else {
            res.sendStatus(404);
        }
    })
}


// PATCH Update Task @listId @taskId
exports.updateTask = async (req, res) => {
    List.findOne({
        _id: req.params.listId,
        _userId: req.user_id,
    })
    .then((list) => {
        if (list) {
            return true;
        }

        // else - the list object is undefined
        return false;
    })
    .then((canUpdateTasks) => {
        if (canUpdateTasks) {
            // the currently authenticated user can update tasks
            Task.findOneAndUpdate({
                _id: req.params.taskId,
                _listId: req.params.listId
            }, {
                    $set: req.body
                }
            ).then(() => {
                res.send({ message: 'Updated successfully.' })
            })
        } else {
            res.sendStatus(404);
        }
    })
}


// DELETE Task @listId @taskId
// Delete specific list with id

exports.deleteTask = async (req, res) => {

    List.findOne({
        _id: req.params.listId,
        _userId: req.user_id
    }).then((list) => {
        if (list) {
            return true;
        }

        return false;
    }).then((canDeleteTasks) => {
        
        if (canDeleteTasks) {
            Task.findOneAndDelete({
                _id: req.params.taskId,
                _listId: req.params.listId
            }).then((removedTaskDoc) => {
                res.send(removedTaskDoc);
            })
        } else {
            res.sendStatus(404);
        }
    });
}